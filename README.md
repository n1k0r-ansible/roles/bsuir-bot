# bsuir-bot role for Ansible

## Variables

```yaml
bsuir_bot:
  tg_token: "123456789:nlHI9YN1vF2OlLuvr7nafJVGM9C8D9BbXTu"
  bot_name: Bot
  student_group: "123456" # bsuir group number
  priveleged_users:
    - 100000000 # telegram user id
    - 200000000

bsuir_bot_front:
  domain: bot.domain.name
  internal_port: 8002
```

Account dictionary is also expected: https://gitlab.com/n1k0r-ansible/roles/master
